package messages;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MessagesServerApp {
    public static void main(String[] args) {
        SpringApplication.run(MessagesServerApp.class, args);
    }
}
