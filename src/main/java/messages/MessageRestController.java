package messages;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/messages")
public class MessageRestController {
    @Autowired
    private MessageRepository messageRepository;

    @GetMapping(value = "{id}",produces = {MediaType.APPLICATION_JSON_UTF8_VALUE,
    MediaType.APPLICATION_XML_VALUE})
    public ResponseEntity<Message> getMessage(
            @PathVariable("id") int id) {
        Message message = messageRepository.getMessageById(id);
        if (message != null){
            return new ResponseEntity<>(message, HttpStatus.OK);
        }else {
            return  new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }
}
